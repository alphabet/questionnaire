import { Meteor } from 'meteor/meteor';
import i18n from 'meteor/universe:i18n';
import SimpleSchema from 'simpl-schema';
import { ValidatedMethod } from 'meteor/mdg:validated-method';

import { getLabel } from '../utils/functions';
import logger from '../logger';

export const setLanguage = new ValidatedMethod({
  name: 'users.setLanguage',
  validate: new SimpleSchema({
    language: { type: String, label: getLabel('api.users.labels.language') },
  }).validator(),

  async run({ language }) {
    if (!this.userId) {
      logger.error({
        message: i18n.__('api.users.mustBeLoggedIn'),
        method: 'users.setLanguage',
        params: { userId: this.userId },
      });
      throw new Meteor.Error('api.users.setLanguage.notPermitted', i18n.__('api.users.mustBeLoggedIn'));
    }
    logger.info({
      message: 'Language set',
      method: 'users.setLanguage',
      params: { userId: this.userId, language: language },
    });
    await Meteor.users.updateAsync(this.userId, {
      $set: { language },
    });
  },
});

export const getIdToken = new ValidatedMethod({
  name: 'users.getIdToken',
  validate: null,
  run() {
    if (!this.userId) {
      throw new Meteor.Error('api.users.getIdToken.notPermitted', i18n.__('api.users.mustBeLoggedIn'));
    }
    // check user existence
    const user = Meteor.users.findOne({ _id: this.userId });
    if (user === undefined) {
      throw new Meteor.Error('api.users.getIdToken.unknownUser', i18n.__('api.users.unknownUser'));
    }
    return user.services?.keycloak?.idToken || null;
  },
});
