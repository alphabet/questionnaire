import { Meteor } from 'meteor/meteor';
import { Random } from 'meteor/random';
import { ValidatedMethod } from 'meteor/mdg:validated-method';
import SimpleSchema from 'simpl-schema';
import i18n from 'meteor/universe:i18n';
import { getLabel } from '../utils';
import logger from '../logger';

import Forms, { Component, Answers } from './forms';

function _createForm(
  title,
  description,
  owner,
  isModel,
  isPublic,
  editableAnswers,
  groups,
  components,
  expirationDate,
  dataDeletionDate,
  personalDataTreatment,
  consignee,
  objective,
  recipients,
  email,
) {
  return Forms.insert({
    title,
    description,
    owner,
    isModel,
    isPublic,
    editableAnswers,
    groups,
    components,
    expirationDate,
    dataDeletionDate,
    personalDataTreatment,
    consignee,
    objective,
    recipients,
    email,
  });
}

function _updateForm(
  id,
  title,
  description,
  owner,
  isModel,
  isPublic,
  editableAnswers,
  groups,
  components,
  expirationDate,
  dataDeletionDate,
  personalDataTreatment,
  consignee,
  objective,
  recipients,
  email,
) {
  Forms.update(
    { _id: id },
    {
      $set: {
        title,
        description,
        owner,
        isModel,
        isPublic,
        editableAnswers,
        groups,
        components,
        expirationDate,
        dataDeletionDate,
        personalDataTreatment,
        consignee,
        objective,
        recipients,
        email,
      },
    },
  );
}

export const createForm = new ValidatedMethod({
  name: 'forms.createForm',
  validate: new SimpleSchema({
    title: { type: String, label: getLabel('api.forms.labels.title') },
    description: { type: String, optional: true, label: getLabel('api.forms.labels.description') },
    isModel: { type: Boolean, label: getLabel('api.forms.labels.isModel') },
    isPublic: { type: Boolean, label: getLabel('api.forms.labels.public') },
    editableAnswers: { type: Boolean, label: getLabel('api.forms.labels.editableAnswers') },
    groups: { type: Array, optional: true, label: getLabel('api.forms.labels.groups') },
    'groups.$': { type: String },
    components: { type: Array, label: getLabel('api.forms.labels.components') },
    'components.$': { type: Component },
    expirationDate: { type: Date },
    dataDeletionDate: { type: Date },
    personalDataTreatment: { type: Boolean, label: getLabel('api.forms.labels.personalDataTreatment') },
    consignee: { type: String, label: getLabel('api.forms.labels.consignee') },
    objective: { type: String, label: getLabel('api.forms.labels.objective') },
    recipients: { type: String, label: getLabel('api.forms.labels.recipients') },
    email: { type: String, label: getLabel('api.forms.labels.email') },
  }).validator(),

  async run({
    title,
    description,
    isModel,
    isPublic,
    editableAnswers,
    groups,
    components,
    expirationDate,
    dataDeletionDate,
    personalDataTreatment,
    consignee,
    objective,
    recipients,
    email,
  }) {
    if (!this.userId) {
      throw new Meteor.Error('api.forms.createForm.noUser', 'api.forms.createForm.notLoggedIn');
    }
    const newId = _createForm(
      title,
      description,
      this.userId,
      isModel,
      isPublic,
      editableAnswers,
      groups,
      components,
      expirationDate,
      dataDeletionDate,
      personalDataTreatment,
      consignee,
      objective,
      recipients,
      email,
    );
    const form = await Forms.findOneAsync({ _id: newId });
    if (!form) {
      logger.error({
        message: i18n.__('api.forms.deleteForm.notExist'),
        method: 'createForm',
        params: { userId: this.userId, formId: newId },
      });
      throw new Meteor.Error('api.forms.createForm.notFound', i18n.__('api.forms.deleteForm.notExist'));
    }
    logger.info({
      message: 'Form created',
      method: 'forms.createForm',
      params: { userId: this.userId, formId: newId },
    });
    return form._id;
  },
});

export const updateForm = new ValidatedMethod({
  name: 'forms.updateForm',
  validate: new SimpleSchema({
    id: { type: String, label: getLabel('api.forms.labels.id') },
    title: { type: String, label: getLabel('api.forms.labels.title') },
    description: { type: String, label: getLabel('api.forms.labels.description') },
    isModel: { type: Boolean, label: getLabel('api.forms.labels.isModel') },
    isPublic: { type: Boolean, label: getLabel('api.forms.labels.public') },
    editableAnswers: { type: Boolean, label: getLabel('api.forms.labels.editableAnswers') },
    groups: { type: Array, optional: true, label: getLabel('api.forms.labels.groups') },
    'groups.$': { type: String },
    components: { type: Array, label: getLabel('api.forms.labels.components') },
    'components.$': { type: Component },
    expirationDate: { type: Date },
    dataDeletionDate: { type: Date },
    personalDataTreatment: { type: Boolean, label: getLabel('api.forms.labels.personalDataTreatment') },
    consignee: { type: String, label: getLabel('api.forms.labels.consignee') },
    objective: { type: String, label: getLabel('api.forms.labels.objective') },
    recipients: { type: String, label: getLabel('api.forms.labels.recipients') },
    email: { type: String, label: getLabel('api.forms.labels.email') },
  }).validator(),

  async run({
    id,
    title,
    description,
    isModel,
    isPublic,
    editableAnswers,
    groups,
    components,
    expirationDate,
    dataDeletionDate,
    personalDataTreatment,
    consignee,
    objective,
    recipients,
    email,
  }) {
    if (!this.userId) {
      logger.error({
        message: i18n.__('api.forms.createForm.notLoggedIn'),
        method: 'forms.updateForm',
        params: { userId: this.userId, formId: id },
      });
      throw new Meteor.Error('api.forms.updateForm.noUser', i18n.__('api.forms.createForm.notLoggedIn'));
    }

    const form = await Forms.findOneAsync({ _id: id });
    if (this.userId !== form.owner) {
      logger.error({
        message: i18n.__('api.forms.createForm.notOwner'),
        method: 'forms.updateForm',
        params: { userId: this.userId, ownerId: form.owner, formId: id },
      });
      throw new Meteor.Error('api.forms.updateForm.permissionDenied', i18n.__('api.forms.deleteForm.notOwner'));
    }

    _updateForm(
      id,
      title,
      description,
      this.userId,
      isModel,
      isPublic,
      editableAnswers,
      groups,
      components,
      expirationDate,
      dataDeletionDate,
      personalDataTreatment,
      consignee,
      objective,
      recipients,
      email,
    );

    logger.info({
      message: 'Form updated',
      method: 'forms.updateForm',
      params: { userId: this.userId, formId: form._id },
    });
    return form._id;
  },
});

export const deleteForm = new ValidatedMethod({
  name: 'forms.deleteForm',
  validate: new SimpleSchema({
    id: { type: String, label: getLabel('api.forms.labels.id') },
  }).validator(),

  async run({ id }) {
    if (!this.userId) {
      logger.error({
        message: i18n.__('api.forms.createForm.notLoggedIn'),
        method: 'forms.deleteForm',
        params: { userId: this.userId, formId: id },
      });
      throw new Meteor.Error('api.forms.deleteForm.noUser', i18n.__('api.forms.createForm.notLoggedIn'));
    }

    const form = await Forms.findOneAsync({ _id: id });
    if (!form) {
      logger.error({
        message: i18n.__('api.forms.deleteForm.notExist'),
        method: 'forms.deleteForm',
        params: { userId: this.userId, formId: id },
      });
      throw new Meteor.Error('api.forms.deleteForm.notFound', i18n.__('api.forms.deleteForm.notExist'));
    }
    if (form.owner !== this.userId) {
      logger.error({
        message: i18n.__('api.forms.deleteForm.notOwner'),
        method: 'forms.deleteForm',
        params: { userId: this.userId, formOwner: form.owner, formId: id },
      });
      throw new Meteor.Error('api.forms.deleteForm.permissionDenied', i18n.__('api.forms.deleteForm.notOwner'));
    }

    logger.info({
      message: 'Form deleted',
      method: 'forms.deleteForm',
      params: { userId: this.userId, formId: id },
    });
    await Forms.removeAsync({ _id: id });
  },
});

export const duplicateForm = new ValidatedMethod({
  name: 'forms.duplicateForm',
  validate: new SimpleSchema({
    _id: { type: String, label: getLabel('api.forms.labels.id') },
  }).validator(),

  async run({ _id }) {
    if (!this.userId) {
      logger.error({
        message: i18n.__('api.forms.createForm.notLoggedIn'),
        method: 'forms.duplicateForm',
        params: { userId: this.userId, formId: _id },
      });
      throw new Meteor.Error('api.forms.duplicateForm.noUser', i18n.__('api.forms.createForm.notLoggedIn'));
    }

    const form = await Forms.findOneAsync({ _id });
    if (!form) {
      logger.error({
        message: i18n.__('api.forms.deleteForm.notExist'),
        method: 'forms.duplicateForm',
        params: { userId: this.userId, formId: _id },
      });
      throw new Meteor.Error('api.forms.duplicateForm.notFound', i18n.__('api.forms.deleteForm.notExist'));
    }
    if (form.owner !== this.userId) {
      logger.error({
        message: i18n.__('api.forms.deleteForm.notOwner'),
        method: 'forms.duplicateForm',
        params: { userId: this.userId, userOwner: form.owner, formId: _id },
      });
      throw new Meteor.Error('api.forms.duplicateForm.permissionDenied', i18n.__('api.forms.deleteForm.notOwner'));
    }

    const today = new Date();

    const newForm = form;
    newForm.title = form.title + ' - Copie';
    newForm.description = form.description || '';

    const expirationDelay = Meteor.settings.public.defaultFormExpirationDelay || 60;
    const deletionDelay = Meteor.settings.public.dataDeletionDelay || 90;

    newForm.expirationDate = new Date(today.setDate(today.getDate() + expirationDelay));
    newForm.dataDeletionDate = new Date(today.setDate(today.getDate() + (expirationDelay + deletionDelay)));

    const newId = _createForm(
      newForm.title,
      newForm.description,
      this.userId,
      newForm.isModel,
      newForm.isPublic,
      newForm.editableAnswers,
      newForm.groups,
      newForm.components,
      newForm.expirationDate,
      newForm.dataDeletionDate,
      newForm.personalDataTreatment,
      newForm.consignee,
      newForm.objective,
      newForm.recipients,
      newForm.email,
    );

    const duplicatedForm = await Forms.findOneAsync({ _id: newId });
    if (!duplicateForm) {
      logger.error({
        message: i18n.__('api.forms.deleteForm.notExist'),
        method: 'forms.deleteForm',
        params: { userId: this.userId, formId: newId },
      });
      throw new Meteor.Error('api.forms.deleteForm.notFound', i18n.__('api.forms.deleteForm.notExist'));
    }

    logger.info({
      message: 'Form duplicated',
      method: 'forms.duplicateForm',
      params: { userId: this.userId, duplicateFormId: duplicatedForm._id },
    });
    return duplicatedForm._id;
  },
});

export const upsertAnswers = new ValidatedMethod({
  name: 'forms.upsertAnswers',
  validate: new SimpleSchema({
    formId: { type: String, label: getLabel('api.forms.labels.id') },
    newAnswer: { type: Answers },
    token: { type: String, optional: true, label: getLabel('api.forms.labels.formAnswers.modifyAnswersToken') },
  }).validator(),

  async run({ formId, newAnswer, token }) {
    const currentForm = await Forms.findOneAsync({ _id: formId });
    if (!currentForm) {
      logger.error({
        message: i18n.__('api.forms.upsertAnswers.notExist'),
        method: 'forms.upsertAnswers',
        params: { formId: formId, currentForm: currentForm },
      });
      throw new Meteor.Error('api.forms.upsertAnswers.notFound', i18n.__('api.forms.upsertAnswers.notExist'));
    }
    if (!currentForm.active) {
      logger.error({
        message: i18n.__('api.forms.upsertAnswers.notActive'),
        method: 'forms.upsertAnswers',
        params: { formId: formId, currentForm: currentForm },
      });
      throw new Meteor.Error('api.forms.upsertAnswers.notActive', i18n.__('api.forms.upsertAnswers.notActive'));
    }
    if (newAnswer.userId !== null && newAnswer.userId !== this.userId) {
      // if new answer userId exists, it must be the connected userId
      logger.error({
        message: i18n.__('api.forms.upsertAnswers.wrongUser'),
        method: 'forms.upsertAnswers',
        params: { formId: formId, userId: newAnswer.userId },
      });
      throw new Meteor.Error('api.forms.upsertAnswers.wrongUser', i18n.__('api.forms.upsertAnswers.wrongUser'));
    }
    if (newAnswer.userId === null && !currentForm.isPublic) {
      // new answer for private forms must have a userId
      logger.error({
        message: i18n.__('api.forms.upsertAnswers.noUserIdFound'),
        method: 'forms.upsertAnswers',
        params: { formId: formId, newAnswer: newAnswer, currentForm: currentForm },
      });
      throw new Meteor.Error('api.forms.upsertAnswers.noUserIdFound', i18n.__('api.forms.upsertAnswers.noUserIdFound'));
    }

    let newTab = currentForm.formAnswers || [];
    let modifyAnswersToken = '';
    if (newAnswer.userId === null && !token) {
      // no userId in answer and no token => first answer for this form
      if (currentForm.editableAnswers) {
        // form has editable answers => generate token
        modifyAnswersToken = Random.secret();
        newTab.push({ ...newAnswer, modifyAnswersToken });
      } else {
        // no token needed
        newTab.push(newAnswer);
      }
    } else if (newAnswer.userId === null && token) {
      // no userId in answer but token given so this form already been answered => find the answer with that token
      const index = newTab.findIndex((answer) => answer.modifyAnswersToken === token);
      if (index === -1) {
        // no answer with this token
        logger.error({
          message: i18n.__('api.forms.upsertAnswers.tokenNotFound'),
          method: 'forms.upsertAnswers',
          params: { formId: formId, newTab: newTab, token: token },
        });
        throw new Meteor.Error(
          'api.forms.upsertAnswers.tokenNotFound',
          i18n.__('api.forms.upsertAnswers.tokenNotFound'),
        );
      } else {
        // answer edited with token
        newTab[index] = { ...newAnswer, modifyAnswersToken: newTab[index].modifyAnswersToken };
        // display modify url again cf. https://gitlab.mim-libre.fr/alphabet/questionnaire/-/issues/226
        modifyAnswersToken = newTab[index].modifyAnswersToken;
      }
    } else {
      // all other cases
      // TO BE REACTOR //
      const index = newTab.findIndex((answer) => answer.userId === newAnswer.userId);
      if (index === -1) {
        newTab.push(newAnswer);
      } else {
        newTab[index] = newAnswer;
      }
      ///////////////////
    }

    logger.info({
      message: 'Answer update / insert',
      method: 'forms.upsertAnswers',
      params: { formId: formId, newTab: newTab, token: token },
    });
    await Forms.updateAsync({ _id: formId }, { $set: { formAnswers: newTab } });
    return modifyAnswersToken;
  },
});

Meteor.methods({
  'forms.clearAnswers': async function (formId) {
    if (!this.userId) {
      logger.error({
        message: i18n.__('api.forms.createForm.notLoggedIn'),
        method: 'forms.clearAnswers',
        params: { formId: formId, userId: this.userId },
      });
      throw new Meteor.Error('api.forms.clearAnswers.notLoggedIn', i18n.__('api.forms.createForm.notLoggedIn'));
    }
    const form = await Forms.findOneAsync({ _id: formId });
    if (form) {
      if (form.owner !== this.userId) {
        logger.error({
          message: i18n.__('api.forms.createForm.notOwner'),
          method: 'forms.clearAnswers',
          params: { formId: formId, userId: this.userId, ownerId: form.ownerId },
        });
        throw new Meteor.Error('api.forms.clearAnswers.permissionDenied', i18n.__('api.forms.deleteForm.notOwner'));
      }
      logger.info({
        message: 'Answer cleared',
        method: 'forms.clearAnswers',
        params: { formId: formId, userId: this.userId },
      });
      await Forms.updateAsync({ _id: formId }, { $set: { formAnswers: [] } });
    }
  },
});

Meteor.methods({
  'forms.toggleActive': async function (formId, active) {
    const form = await Forms.findOneAsync({ _id: formId });
    if (form) {
      if (form.owner == this.userId) {
        form.active = active;
        logger.info({
          message: 'Form activated',
          method: 'forms.toggleActive',
          params: { formId: formId, userId: this.userId },
        });
        await Forms.updateAsync({ _id: formId }, { $set: { active: form.active } });
        if (Meteor.isServer && form.groups.length && form.active && !Meteor.isTest) {
          // eslint-disable-next-line global-require
          const sendnotif = require('../notifications/server/notifSender').default;

          sendnotif({
            groups: form.groups,
            title: 'Nouveau questionnaire',
            content: `Le questionnaire ${form.title} a été créé pour votre groupe`,
            formId: form._id,
          });
        }
      } else {
        logger.error({
          message: i18n.__('api.forms.deleteForm.notOwner'),
          method: 'forms.toggleActive',
          params: { formId: formId, userId: this.userId, ownerId: form.owner },
        });
        throw new Meteor.Error('api.forms.toggleActive.permissionDenied', i18n.__('api.forms.deleteForm.notOwner'));
      }
    } else {
      logger.error({
        message: i18n.__('api.forms.deleteForm.notExist'),
        method: 'forms.toggleActive',
        params: { formId: formId, userId: this.userId },
      });
      throw new Meteor.Error('api.forms.toggleActive.notExist', i18n.__('api.forms.deleteForm.notExist'));
    }
  },
});

Meteor.methods({
  'forms.getOne': async (id) => {
    const form = await Forms.findOneAsync({ _id: id });
    if (!form) {
      logger.error({
        message: i18n.__('api.forms.deleteForm.notExist'),
        method: 'forms.getOne',
        params: { formId: id, userId: this.userId },
      });
      throw new Meteor.Error('api.forms.deleteForm.notFound', i18n.__('api.forms.deleteForm.notExist'));
    }
    const owner = form ? await Meteor.users.findOneAsync({ _id: form.owner }) : { firstName: '', lastName: '' };
    logger.info({
      message: 'Form getter',
      method: 'forms.getOne',
      params: { formId: id, userId: this.userId },
    });
    return { form, owner: { firstName: owner.firstName, lastName: owner.lastName } };
  },
});

export const getOneFormFromuser = new ValidatedMethod({
  name: 'forms.getOneFromUser',
  validate: null,

  async run(id) {
    if (!id) {
      return null;
    }
    if (!this.userId) {
      logger.error({
        message: i18n.__('api.forms.createForm.notLoggedIn'),
        method: 'forms.getOneFromUser',
        params: { formId: id, userId: this.userId },
      });
      throw new Meteor.Error('api.forms.getOneFromUser.noUser', i18n.__('api.forms.createForm.notLoggedIn'));
    }

    const form = await Forms.findOneAsync({ _id: id });
    if (form.owner !== this.userId) {
      logger.error({
        message: i18n.__('api.forms.createForm.notOwner'),
        method: 'forms.getOneFromUser',
        params: { formId: id, userId: this.userId, ownerId: form.owner },
      });
      throw new Meteor.Error('api.forms.getOneFromUser.permissionDenied', i18n.__('api.forms.deleteForm.notOwner'));
    }

    logger.info({
      message: 'Form getter from user',
      method: 'forms.getOneFromUser',
      params: { formId: id, userId: this.userId },
    });
    return form;
  },
});

Meteor.methods({
  'forms.getAll': async () => {
    const res = await Forms.find().mapAsync((form) => form);
    logger.info({
      message: 'Get all forms',
      method: 'forms.getAll',
      params: { userId: this.userId },
    });
    return res;
  },
});

export const getUserForms = new ValidatedMethod({
  name: 'forms.getUserForms',
  validate: null,

  async run() {
    if (this.userId) {
      const res = await Forms.find({ owner: this.userId }).mapAsync((form) => form);
      logger.info({
        message: 'Get user forms',
        method: 'forms.getUserForms',
        params: { userId: this.userId },
      });
      return res;
    } else {
      logger.error({
        message: i18n.__('api.forms.createForm.notLoggedIn'),
        method: 'forms.getUserForms',
        params: { userId: this.userId },
      });
      throw new Meteor.Error('api.forms.getUserForms.noUser', i18n.__('api.forms.createForm.notLoggedIn'));
    }
  },
});
