import { Meteor } from 'meteor/meteor';
import * as winston from 'winston';

const { combine, timestamp, json, colorize, align, printf } = winston.format;

// eslint-disable-next-line no-unused-vars
const infoParams = ({ timestamp, level, message, ...info }) => info;

const fileFormatDev = combine(
  colorize({ all: true }),
  printf((info) => `${info.timestamp} [${info.level}] ${info.message} ${JSON.stringify(infoParams(info))}`),
  align(),
);

const fileFormat = combine(timestamp({ format: 'YYYY-MM-DD HH:mm:ss.SSS' }), json());

console.log('Initialize logger');

/*
Logs levels:
  emerg: 0,
  alert: 1,
  crit: 2,
  error: 3,
  warning: 4,
  notice: 5,
  info: 6,
  debug: 7
*/

const logger = winston.createLogger({
  levels: winston.config.syslog.levels,
  transports: [
    new winston.transports.Console({
      level: 'info',
      format: Meteor.isDevelopment ? combine(fileFormat, fileFormatDev) : fileFormat,
      silent: Meteor.isTest || Meteor.isClient,
    }),
  ],
});

// logger.<level>({ message: '', method: '', params: {} });

export default logger;
