import React from 'react';
import RadioButtonCheckedIcon from '@mui/icons-material/RadioButtonChecked';
import KeyboardArrowDownIcon from '@mui/icons-material/KeyboardArrowDown';
import InsertPageBreakIcon from '@mui/icons-material/InsertPageBreak';
import CheckBoxIcon from '@mui/icons-material/CheckBox';
import MoneyIcon from '@mui/icons-material/Money';
import ShortTextIcon from '@mui/icons-material/ShortText';
import NotesIcon from '@mui/icons-material/Notes';
import CalendarMonthIcon from '@mui/icons-material/CalendarMonth';
import TextsmsIcon from '@mui/icons-material/Textsms';
import StarRateIcon from '@mui/icons-material/StarRate';
import AddCardIcon from '@mui/icons-material/AddCard';
import HorizontalRuleIcon from '@mui/icons-material/HorizontalRule';

export const LIST_OF_INPUT_BUILDER = [
  {
    id: 'radioButtonInput',
    name: 'radioButton',
    icon: <RadioButtonCheckedIcon />,
  },
  {
    id: 'selectInput',
    name: 'select',
    icon: <KeyboardArrowDownIcon />,
  },
  {
    id: 'checkboxInput',
    name: 'checkbox',
    icon: <CheckBoxIcon />,
  },
  {
    id: 'numberInput',
    name: 'number',
    icon: <MoneyIcon />,
  },
  {
    id: 'textInput',
    name: 'text',
    icon: <ShortTextIcon />,
  },
  {
    id: 'textArea',
    name: 'textArea',
    icon: <NotesIcon />,
  },
  {
    id: 'dateInput',
    name: 'date',
    icon: <CalendarMonthIcon />,
  },
  {
    id: 'ratingButtonInput',
    name: 'ratingButton',
    icon: <StarRateIcon />,
  },
  {
    id: 'comment',
    name: 'comment',
    icon: <TextsmsIcon />,
  },
  {
    id: 'sectionStart',
    name: 'sectionStart',
    icon: <AddCardIcon />,
  },
  {
    id: 'separator',
    name: 'separator',
    icon: <HorizontalRuleIcon />,
  },
  {
    id: 'pageBreak',
    name: 'pageBreak',
    icon: <InsertPageBreakIcon />,
  },
];
