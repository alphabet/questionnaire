import React from 'react';
import i18n from 'meteor/universe:i18n';
import { Chart as ChartJS, ArcElement, Tooltip, Legend } from 'chart.js';
import { Pie } from 'react-chartjs-2';
import { generateColor } from '../utils/utils';

export default function StatsPie({ question }) {
  ChartJS.register(ArcElement, Tooltip, Legend);

  const getAllCountStat = (stats) => {
    let cpt = 0;
    stats.map((stat) => {
      cpt += stat.count;
    });
    return cpt;
  };

  const generateChartData = (stat) => {
    const colors = [];
    for (let i = 0; i < stat.length; i++) {
      const color = generateColor();
      colors.push(color);
    }

    const chartData = {
      labels: stat.map((oneStat) =>
        oneStat.answer === undefined ? i18n.__('component.answerListDisplay.emptyAnswer') : oneStat.answer,
      ),
      datasets: [
        {
          label: 'Nombre de réponses: ',
          data: stat.map((oneStat) => oneStat.count),
          backgroundColor: colors.map((col) => col),
          borderWidth: 1,
        },
      ],

      options: {
        plugins: {
          legend: {
            position: 'bottom',
          },
        },
      },
    };

    return chartData;
  };

  // Permet de générer les graph en bar
  const choicesStats = {};

  question.questionChoices.forEach((key) => (choicesStats[key] = 0));
  question.stat.forEach((element) => {
    element.answer === undefined
      ? choicesStats[i18n.__('component.answerListDisplay.emptyAnswer')]++
      : choicesStats[element.answer]++;
  });
  // ********************************

  const displayAnswer = (answer) => {
    if (answer === undefined) return i18n.__('component.answerListDisplay.emptyAnswer');
    if (answer instanceof Array) return answer.join(' - ');
    return answer;
  };

  return (
    <div
      style={{
        display: 'flex',
        height: '35vh',
        justifyContent: 'space-between',
      }}
    >
      <div
        style={{
          display: 'flex',
          flexDirection: 'column',
          overflowY: 'auto',
          scrollbarWidth: 'thin',
        }}
      >
        {question.stat.map((oneStat) => (
          <div key={oneStat.answer} style={{ marginBottom: '10px', paddingRight: '10px' }}>
            {displayAnswer(oneStat.answer)} : {((oneStat.count / getAllCountStat(question.stat)) * 100).toFixed(2)}%
          </div>
        ))}
      </div>
      <div>
        <Pie data={generateChartData(question.stat)} />
      </div>
    </div>
  );
}
