import React from 'react';
import i18n from 'meteor/universe:i18n';
import { useNavigate } from 'react-router-dom';

import { BottomNavigation, Button } from '@mui/material';
import { useSelector } from 'react-redux';
import { formOnlyContainsLayoutComponent } from '../../utils/utils';

export const Footer = ({ nextStep, urlOfPrevStep, text }) => {
  const bottomBarStyle = {
    display: 'flex',
    alignItems: 'center',
    height: '8vh',
    backgroundColor: 'white',
    padding: '0 15px',
  };

  const today = new Date();
  const navigate = useNavigate();
  const form = useSelector((state) => state.form);

  const isDisable = !form.title || form.components.length === 0 || formOnlyContainsLayoutComponent(form);
  const isTitleInValid = !form.title || form.title.length > 96;
  const isDescriptionInValid = form.description.length > 0 && form.description.length > 256;
  const wrongExpirationDate = today > form.expirationDate;

  const rgpdIsInvalid =
    form.personalDataTreatment &&
    (!form.objective ||
      form.objective.length === 0 ||
      !form.consignee ||
      form.consignee.length === 0 ||
      !form.email ||
      form.email.length === 0);

  return (
    <div style={{ position: 'fixed', bottom: 0, left: '37vw' }}>
      <BottomNavigation sx={bottomBarStyle}>
        <div style={{ display: 'flex', justifyContent: 'center' }}>
          {urlOfPrevStep ? (
            urlOfPrevStep.includes('intro') ? (
              <>
                <Button variant="contained" sx={{ marginRight: '5vw' }} onClick={() => navigate(`/${urlOfPrevStep}`)}>
                  {i18n.__('component.footer.goBack')}
                </Button>
                <Button variant="contained" disabled={rgpdIsInvalid || !form.title} onClick={() => nextStep()}>
                  {text}
                </Button>
              </>
            ) : (
              <>
                <Button variant="contained" sx={{ marginRight: '5vw' }} onClick={() => navigate(`/${urlOfPrevStep}`)}>
                  {i18n.__('component.footer.goBack')}
                </Button>
                <Button variant="contained" disabled={isDisable || rgpdIsInvalid} onClick={() => nextStep()}>
                  {text}
                </Button>
              </>
            )
          ) : (
            <>
              <Button variant="contained" sx={{ marginRight: '5vw' }} onClick={() => navigate(`/`)}>
                {i18n.__('component.footer.cancel')}
              </Button>
              <Button
                variant="contained"
                disabled={isTitleInValid || isDescriptionInValid || wrongExpirationDate}
                onClick={() => nextStep()}
              >
                {text}
              </Button>
            </>
          )}
        </div>
      </BottomNavigation>
    </div>
  );
};
