import React, { useState } from 'react';
import i18n from 'meteor/universe:i18n';
import { Modal, Box, Typography, Button } from '@mui/material';
import { useNavigate } from 'react-router-dom';
import { useSelector } from 'react-redux';

const style = {
  position: 'absolute',
  top: '50%',
  left: '50%',
  transform: 'translate(-50%, -50%)',
  width: '50vw',
  bgcolor: 'background.paper',
  borderRadius: 5,
  p: 4,
  overflowX: 'hidden',
  overflowY: 'scroll',
  height: '70%',
  scrollbarColor: 'grey white',
  scrollbarWidth: 'auto',
};

const ModalRgpd = () => {
  const navigate = useNavigate();
  const [openModal, setOpenModal] = useState(true);
  const form = useSelector((state) => state.form);

  const handleAccept = () => {
    setOpenModal(false);
  };

  const handleReject = () => {
    navigate('/');
  };

  return (
    <Modal open={openModal} aria-labelledby="modal-modal-title" aria-describedby="modal-modal-description">
      <Box sx={style}>
        <Typography id="modal-modal-title" variant="h6" component="h2">
          {i18n.__('component.rgpd.acceptation')}
        </Typography>

        <Typography sx={{ mt: 2 }}>
          Les informations recueillies dans le cadre de ce questionnaire sont enregistrées dans un fichier informatisé
          mis en œuvre par
          <b> {form.consignee}</b> dont la finalité est :
          <br />
          <b> {form.objective}</b>
          <br />
          <br /> Ce questionnaire constitue un traitement de données à caractère personnel dont la base légale est
          l’exécution d’une mission d’intérêt public au sens de l’article 6 du règlement général (UE) 2016/679 du
          Parlement européen et du Conseil du 27 avril 2016 sur la protection des données (RGPD).
          <br />
          Les données sont conservées pendant <b>{Meteor.settings.public.dataDeletionDelay || 90} jours</b>.
          <br />
          <br /> Les destinataires des données à caractère personnel sont :
          <ul>
            {form.recipients && <li>Les agents habilités : {form.recipients}</li>}
            <li>Les membres du Pôle Enquêtes de la Direction du Numérique pour l’Éducation nationale</li>
            <li>Les membres du Pôle de Compétences Logiciel Libre du Ministère de l’Éducation nationale</li>
          </ul>
          Vous pouvez accéder aux données vous concernant et exercer vos droits d’accès, de rectification, de
          limitation, d’opposition que vous tenez des articles 15, 16, 18 et 21 du RGPD, par courriel à {form.email}.
          <br />
          De la même manière, vous pouvez exercer les droits prévus à l’article 85 de la loi n° 78-17 du 6 janvier 1978
          relative à l’informatique, aux fichiers et aux libertés.
          <br />
          <br />
          Pour toute question sur le traitement de vos données dans ce dispositif, vous pouvez contacter le délégué à la
          protection des données du ministère de l’Éducation nationale et de la Jeunesse :
          <ul>
            <li>à l’adresse électronique suivante : dpd@education.gouv.fr</li>
            <li>
              via le{' '}
              <a
                href="https://www.education.gouv.fr/contactez-nous-41633/category/taxonomy/term/113264"
                target="_blank"
                rel="noopener noreferrer"
              >
                formulaire de saisie en ligne
              </a>
            </li>
            <li>
              ou par courrier en vous adressant au : ministère de l’Éducation nationale et de la Jeunesse - À
              l’attention du délégué à la protection des données (DPD) - 110, rue de Grenelle - 75357 Paris Cedex 07
            </li>
          </ul>
          Si vous estimez, après nous avoir contactés, que vos droits en matière de protection des données à caractère
          personnel ne sont pas respectés, vous pouvez adresser une réclamation auprès de la Commission nationale de
          l’informatique et des libertés (CNIL) à l’adresse suivante : 3 Place de Fontenoy – TSA 80715 – 75334 Paris
          Cedex 07.
          <br />
          <br />
          Dans le cadre de l’exercice de vos droits, vous devez justifier de votre identité par tout moyen. En cas de
          doute sur votre identité, les services chargés du droit d’accès et le délégué à la protection des données se
          réservent le droit de vous demander les informations supplémentaires qui leur apparaissent nécessaires, y
          compris la photocopie d’un titre d’identité portant votre signature.
        </Typography>

        <div style={{ display: 'flex', marginTop: '2vh', justifyContent: 'space-between' }}>
          <Button variant="contained" onClick={handleReject}>
            {i18n.__('component.modalRgpd.refuse')}
          </Button>
          <Button variant="contained" onClick={handleAccept}>
            {i18n.__('component.modalRgpd.accept')}
          </Button>
        </div>
      </Box>
    </Modal>
  );
};

export default ModalRgpd;
