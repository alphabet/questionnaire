import React, { useEffect, useState } from 'react';
import i18n from 'meteor/universe:i18n';
import { Link } from 'react-router-dom';
import { AppBar, Slide, useScrollTrigger } from '@mui/material';
import { HeaderMenu } from './HeaderMenu';
import LanguageSwitcher from './LanguageSwitcher';

const appBarStyle = {
  display: 'flex',
  flexDirection: 'row',
  justifyContent: 'space-between',
  alignItems: 'center',
  paddingLeft: '2%',
  paddingRight: '2%',
  height: '60px',
  backgroundColor: '#F9F9FD',
};

export const Header = () => {
  let [theme, setTheme] = useState('eole');

  useEffect(() => {
    Meteor.call('contextsettings.getTheme', (e, r) => {
      if (e) {
        console.log(e.message);
      } else {
        setTheme(r[0]?.value[0]);
      }
    });
  }, []);

  function themeLogo(theme) {
    switch (theme) {
      case 'laboite':
        return '/images/laboite/Logo-A.svg';
      case 'eole':
        return '/images/puce_eole.png';
      case 'mim':
        return '/images/mim/logo-9.svg';
      default:
        return '/images/puce_eole.png';
    }
  }

  const trigger = useScrollTrigger();

  return (
    <Slide appear={false} direction="down" in={!trigger}>
      <AppBar sx={appBarStyle}>
        <Link to="/" style={{ textDecoration: 'none', display: 'flex' }}>
          <img src={themeLogo(theme)} alt="app logo" style={{ height: 40, marginTop: '20px', paddingRight: '1vh' }} />
          <h1 style={{ fontFamily: 'OdinRounded', color: '#372f84' }}>{i18n.__('component.header.title')}</h1>
        </Link>
        <div style={{ display: 'flex' }}>
          <HeaderMenu />
          <LanguageSwitcher />
        </div>
      </AppBar>
    </Slide>
  );
};
