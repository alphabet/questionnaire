import React, { useContext, useState } from 'react';
import i18n from 'meteor/universe:i18n';
import { useNavigate, useLocation } from 'react-router-dom';
// eslint-disable-next-line no-unused-vars
import { identicon } from 'minidenticons'; // Don't delete this import it's for default avatar
import { Button, Typography, Menu, MenuItem, Avatar, Divider } from '@mui/material';
import ExpandMoreIcon from '@mui/icons-material/ExpandMore';
import { UserContext } from '../../contexts/UserContext';
import { openInNewTab } from '../../utils/utils';
import LogoutDialog from '../modals/LogoutDialog';

// CSS style
const sizeAvatar = {
  width: 40,
  height: 40,
};

export const HeaderMenu = () => {
  const [open, setOpen] = useState(false);
  const [anchor, setAnchor] = useState(null);
  const [openLogout, setOpenLogout] = useState(false);
  const { user } = useContext(UserContext);

  const navigate = useNavigate();
  const location = useLocation();

  const openMenu = (event) => {
    setAnchor(event.currentTarget);
    setOpen(!open);
  };

  const keycloakLogout = () => {
    const { keycloakUrl, keycloakRealm } = Meteor.settings.public;
    const keycloakLogoutUrl = `${keycloakUrl}/realms/${keycloakRealm}/protocol/openid-connect/logout`;
    const redirectUri = Meteor.absoluteUrl('/logout');

    Meteor.call('users.getIdToken', {}, (err, res) => {
      if (err) window.location = `${keycloakLogoutUrl}?post_logout_redirect_uri=${redirectUri}`;
      else {
        // if id token has been retrieved, send it as id_token_hint in logout request
        const tokenHintPart = !res ? '' : `&id_token_hint=${res}`;
        window.location = `${keycloakLogoutUrl}?post_logout_redirect_uri=${redirectUri}${tokenHintPart}`;
      }
    });
  };

  const localLogout = () => {
    navigate('/logout');
    setOpen(!open);
  };

  const logout = () => {
    const logoutType = user.logoutType || 'ask';
    if (logoutType === 'ask') {
      setOpenLogout(true);
    } else if (logoutType === 'global') {
      keycloakLogout();
    } else localLogout();
  };

  const closeLogoutDialog = () => {
    setOpenLogout(false);
    setAnchor(null);
  };

  const about = () => {
    navigate('/about');
    setOpen(!open);
  };

  const handleOpenInNewTab = () => {
    openInNewTab('/about');
    setOpen(!open);
  };

  const login = () => {
    // redirect to home if on signin page, optherwise stay on current page
    if (location.pathname === '/signin') navigate('/');
    Meteor.loginWithKeycloak();
  };

  if (!user)
    return (
      <Button variant="outlined" onClick={login}>
        {i18n.__('component.headerMenu.login')}
      </Button>
    );

  return (
    <div>
      <Button
        endIcon={<ExpandMoreIcon fontSize="large" />}
        style={{ textTransform: 'none' }}
        onClick={(event) => openMenu(event)}
      >
        <Typography variant="body1" sx={{ marginRight: '1vw' }}>
          {user.username || i18n.__('api.users.labels.noUsername')}
        </Typography>
        <div>
          {user.avatar ? (
            <Avatar sx={sizeAvatar} src={user.avatar} alt={user.username} />
          ) : (
            <div style={sizeAvatar}>
              <identicon-svg style={sizeAvatar} username="default" />
            </div>
          )}
        </div>
      </Button>
      <Menu anchorEl={anchor} open={open} onClick={() => setOpen(!open)}>
        <MenuItem onClick={logout}>{i18n.__('component.headerMenu.logout')}</MenuItem>
        <Divider />
        <MenuItem onClick={about} onAuxClick={handleOpenInNewTab}>
          {i18n.__('component.headerMenu.about')}
        </MenuItem>
      </Menu>
      <LogoutDialog open={openLogout} onClose={closeLogoutDialog} onAccept={keycloakLogout} />
    </div>
  );
};
