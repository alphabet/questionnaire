import React, { useState } from 'react';
import i18n from 'meteor/universe:i18n';
import { useNavigate } from 'react-router-dom';
import { Stepper, Step, StepButton } from '@mui/material';
import { useEffect } from 'react';
import { useSelector } from 'react-redux';
import { formOnlyContainsLayoutComponent } from '../../utils/utils';

export const Breadcrumb = () => {
  const [activeStep, setActiveStep] = useState(0);

  const form = useSelector((state) => state.form);

  const isTitleInValid = !form.title || form.title.length > 96;
  const isDescriptionInValid = form.description.length > 0 && form.description.length > 256;

  const isValidRGPD = !form.personalDataTreatment;

  const today = new Date();
  const isExpirationDateInValid = today > form.expirationDate;
  const isFormComponentsInvalid =
    !form.components || form.components?.length === 0 || formOnlyContainsLayoutComponent(form);

  const navigate = useNavigate();

  const navigateTo = (step) => {
    navigate(`/builder/${step}`);
  };
  const url = window.location.href;

  useEffect(() => {
    if (url.includes('intro')) {
      setActiveStep(0);
    } else if (url.includes('components')) {
      form.personalDataTreatment ? setActiveStep(2) : setActiveStep(1);
    } else if (url.includes('rgpd')) {
      setActiveStep(1);
    } else if (url.includes('previsualizer')) {
      form.personalDataTreatment ? setActiveStep(3) : setActiveStep(2);
    }
  }, [url]);

  return (
    <div style={{ display: 'flex', justifyContent: 'center', width: '100%' }}>
      <div style={{ marginBottom: '6vh', width: '60vw' }}>
        <Stepper alternativeLabel activeStep={activeStep}>
          <Step key="intro" completed={activeStep === 1 || activeStep === 2 || activeStep === 3}>
            <StepButton
              disabled={isTitleInValid || isDescriptionInValid || isExpirationDateInValid}
              color="inherit"
              onClick={() => navigateTo('intro')}
            >
              {i18n.__('component.breadcrumb.intro')}
            </StepButton>
          </Step>
          {form.personalDataTreatment && (
            <Step key="rgpd" completed={activeStep === 2 || activeStep === 3} disabled={!form.title}>
              <StepButton
                disabled={isTitleInValid || isDescriptionInValid || isExpirationDateInValid}
                color="inherit"
                onClick={() => navigateTo('rgpd')}
              >
                {i18n.__('component.breadcrumb.rgpd')}
              </StepButton>
            </Step>
          )}
          <Step
            key="components"
            completed={!form.personalDataTreatment ? activeStep === 2 : activeStep === 3}
            disabled={!form.title}
          >
            <StepButton
              disabled={isTitleInValid || isDescriptionInValid || isExpirationDateInValid || isValidRGPD}
              color="inherit"
              onClick={() => navigateTo('components')}
            >
              {i18n.__('component.breadcrumb.components')}
            </StepButton>
          </Step>
          <Step key="previsualizer" disabled={form.components.length === 0}>
            <StepButton
              disabled={isTitleInValid || isDescriptionInValid || isExpirationDateInValid || isFormComponentsInvalid}
              color="inherit"
              onClick={() => navigateTo('previsualizer')}
            >
              {i18n.__('component.breadcrumb.previsualization')}
            </StepButton>
          </Step>
        </Stepper>
      </div>
    </div>
  );
};
