import { IconButton } from '@mui/material';
import { i18n } from 'meteor/universe:i18n';
import React, { useContext, useState } from 'react';

import LockIcon from '@mui/icons-material/Lock';
import LockOpenIcon from '@mui/icons-material/LockOpen';
import FactCheckIcon from '@mui/icons-material/FactCheck';
import BorderColorIcon from '@mui/icons-material/BorderColor';
import EditIcon from '@mui/icons-material/Edit';

import Alert from '@mui/material/Alert';
import Snackbar from '@mui/material/Snackbar';
import { useNavigate } from 'react-router-dom';
import {
  toggleActiveForm,
  copyUrlToClipBoard,
  hasNotAnswers,
  expirationDateIsPassed,
  hasAlreadyRespond,
  openInNewTab,
} from '../utils/utils';
import DeleteIcon from '@mui/icons-material/Delete';
import LinkIcon from '@mui/icons-material/Link';
import ContentCopyIcon from '@mui/icons-material/ContentCopy';
import Slide from '@mui/material/Slide';
import { UserContext } from '../contexts/UserContext';
import ModalDeleteConfirmation from './modals/ModalDeleteConfirmation';

export const FormActionButton = ({ deleteForm, currentForm }) => {
  const navigate = useNavigate();
  const [active, setActive] = useState(currentForm.active);
  const [open, setOpen] = useState(false);
  const [openDeleteModal, setOpenDeleteModal] = useState(false);
  const { user } = useContext(UserContext);

  const activeForm = () => {
    setActive(!active);
    toggleActiveForm(currentForm);
  };

  const handleDeleteForm = () => {
    setOpenDeleteModal(true);
  };

  const alreadyRespond = () => {
    return !currentForm?.editableAnswers && hasAlreadyRespond(user, currentForm);
  };

  const handleCopyClipboard = (id) => {
    copyUrlToClipBoard(id);
    setOpen(true);
  };

  const handleCopyForm = async () => {
    try {
      const result = await Meteor.callAsync('forms.duplicateForm', {
        _id: currentForm._id,
      });

      if (result) {
        navigate(`/builder/intro/${result}?duplicate=true`);
      }
    } catch (err) {
      console.log('error dans updateForm', err);
    }
  };

  return (
    <div style={{ flexDirection: 'column', flex: 1 }}>
      {open && (
        <Snackbar
          open={open}
          autoHideDuration={6000}
          onClose={() => setOpen(false)}
          anchorOrigin={{ vertical: 'top', horizontal: 'right' }}
          TransitionComponent={Slide}
          sx={{ marginTop: '5vh' }}
        >
          <Alert onClose={() => setOpen(false)} severity="success" sx={{ width: '100%' }}>
            {i18n.__('component.formActionButton.copyUrlSuccess')}
          </Alert>
        </Snackbar>
      )}
      <IconButton
        title={
          active ? i18n.__('component.formActionButton.isActive') : i18n.__('component.formActionButton.isNotActive')
        }
        disabled={expirationDateIsPassed(currentForm)}
        onClick={() => activeForm()}
      >
        {active ? <LockOpenIcon /> : <LockIcon />}
      </IconButton>
      <IconButton
        title={i18n.__('component.formActionButton.answer')}
        disabled={hasNotAnswers(currentForm)}
        onClick={() => navigate(`/answers/${currentForm._id}`)}
        onAuxClick={() => openInNewTab(`/answers/${currentForm._id}`)}
      >
        <FactCheckIcon />
      </IconButton>
      <IconButton
        title={i18n.__('component.formActionButton.editAnswers')}
        disabled={!active || alreadyRespond() || expirationDateIsPassed(currentForm)}
        onClick={() => navigate(`/visualizer/${currentForm._id}`)}
        onAuxClick={() => openInNewTab(`/visualizer/${currentForm._id}`)}
      >
        <BorderColorIcon />
      </IconButton>
      <IconButton
        title={i18n.__('component.formActionButton.copyUrl')}
        onClick={() => handleCopyClipboard(currentForm._id)}
        disabled={!active || expirationDateIsPassed(currentForm)}
      >
        <LinkIcon />
      </IconButton>
      <IconButton
        title={i18n.__('component.formActionButton.editForm')}
        disabled={active || !hasNotAnswers(currentForm)}
        onClick={() => navigate(`/builder/intro/${currentForm._id}`)}
        onAuxClick={() => openInNewTab(`/builder/intro/${currentForm._id}`)}
      >
        <EditIcon />
      </IconButton>
      <IconButton title={i18n.__('component.formActionButton.copyForm')} onClick={() => handleCopyForm()}>
        <ContentCopyIcon />
      </IconButton>
      <IconButton title={i18n.__('component.formActionButton.deleteForm')} onClick={() => handleDeleteForm()}>
        <DeleteIcon />
      </IconButton>
      {openDeleteModal ? (
        <ModalDeleteConfirmation
          deleteForm={deleteForm}
          form={currentForm}
          open={openDeleteModal}
          onClose={() => {
            setOpenDeleteModal(false);
          }}
        />
      ) : null}
    </div>
  );
};
