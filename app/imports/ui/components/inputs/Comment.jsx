import React from 'react';
import { Paper, Typography } from '@mui/material';
import { styles } from './InputsStyles.jsx';

export const Comment = ({ title }) => {
  return (
    <Paper sx={styles.paperSize}>
      <Typography sx={{ whiteSpace: 'pre-wrap' }}>{title}</Typography>
    </Paper>
  );
};
