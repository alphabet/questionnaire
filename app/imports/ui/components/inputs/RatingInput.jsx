import React from 'react';
import { FormLabel, FormControl, Rating, Paper } from '@mui/material';
import { addAnswers } from '../../redux/slices/answerFormSlice';
import { useDispatch, useSelector } from 'react-redux';
import { styles } from './InputsStyles.jsx';

export const RatingInput = ({ title, questionId, answerRequired }) => {
  const dispatch = useDispatch();
  const inputAnswer = useSelector((state) =>
    state.answerForm.answers.find((answer) => answer.questionId === questionId),
  );

  const validateAnswer = (event) => {
    dispatch(addAnswers({ questionId, value: event.target.value }));
  };

  return (
    <Paper sx={styles.paperSize}>
      <FormControl required={answerRequired} error={answerRequired && !inputAnswer}>
        <FormLabel sx={styles.labelColor}>{title}</FormLabel>
        <Rating value={Number(inputAnswer?.answer) || null} onChange={(e) => validateAnswer(e)}></Rating>
      </FormControl>
    </Paper>
  );
};
