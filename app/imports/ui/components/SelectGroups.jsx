import { Button, FormControl, InputLabel, MenuItem, Select } from '@mui/material';
import React, { useEffect, useState } from 'react';
import { i18n } from 'meteor/universe:i18n';
import { useDispatch, useSelector } from 'react-redux';
import { addGroups } from '../redux/slices/formSlice';
import { getStrucGroupName } from '../../api/groups/methods';

export default function SelectGroups({ userGroups }) {
  const [groupSelected, setGroupSelected] = useState({});
  const dispatch = useDispatch();
  const form = useSelector((state) => state.form);

  const displayGroupsNotSelected = () => {
    return userGroups.filter((group) => form.groups.findIndex((groupId) => groupId === group._id) === -1);
  };

  const selectGroup = (value) => {
    const index = userGroups.findIndex((group) => group.name === value);
    if (index !== -1) return setGroupSelected(userGroups[index]);
  };

  const addGroupToForm = () => {
    if (Object.keys(groupSelected).length !== 0) {
      dispatch(addGroups(groupSelected._id));
    }
  };

  useEffect(() => {
    const pageHeight = document.documentElement.scrollHeight;
    window.scroll(0, pageHeight);
  }, [form.groups]);

  const haveNotGroup = !userGroups || userGroups.length <= 0;

  if (haveNotGroup) return <p>{i18n.__('component.selectGroups.noGroup')}</p>;

  return (
    <div style={{ display: 'flex', gap: 10, alignItems: 'center', marginTop: '1vh' }}>
      <div style={{ display: 'flex', flexDirection: 'column', width: '100%' }}>
        <FormControl>
          <InputLabel id="selectInput-Groups">{i18n.__('component.selectGroups.groupChoice')}</InputLabel>
          <Select
            labelId="selectInput-Groups"
            label={i18n.__('component.selectGroups.groupChoice')}
            value={groupSelected.name}
            onChange={(event) => {
              selectGroup(event.target.value);
            }}
          >
            {displayGroupsNotSelected().map((group) => (
              <MenuItem key={group._id} value={group.name}>
                {getStrucGroupName(group)}
              </MenuItem>
            ))}
          </Select>
        </FormControl>
      </div>
      <Button variant="contained" onClick={() => addGroupToForm()} sx={{ height: '3.5rem', width: '10vw' }}>
        {i18n.__('component.selectGroups.addGroup')}
      </Button>
    </div>
  );
}
