import { Meteor } from 'meteor/meteor';
import React, { useState } from 'react';
import { useNavigate } from 'react-router-dom';
import i18n from 'meteor/universe:i18n';
import PropTypes from 'prop-types';
import Button from '@mui/material/Button';
import Dialog from '@mui/material/Dialog';
import DialogActions from '@mui/material/DialogActions';
import DialogContent from '@mui/material/DialogContent';
import DialogContentText from '@mui/material/DialogContentText';
import DialogTitle from '@mui/material/DialogTitle';
import FormGroup from '@mui/material/FormGroup';
import FormControlLabel from '@mui/material/FormControlLabel';
import Checkbox from '@mui/material/Checkbox';

function LogoutDialog({ open, onAccept, onClose }) {
  const [dontAsk, setDontAsk] = useState(false);
  const navigate = useNavigate();
  const simpleLogout = () => {
    if (dontAsk) Meteor.call('users.setLogoutType', { logoutType: 'local' });
    onClose();
    navigate('/logout');
  };
  const keycloakLogout = () => {
    if (dontAsk) Meteor.call('users.setLogoutType', { logoutType: 'global' });
    onAccept();
  };

  return (
    <Dialog
      open={open}
      keepMounted
      onClose={onClose}
      aria-labelledby="alert-dialog-slide-title"
      aria-describedby="alert-dialog-slide-description"
    >
      <DialogTitle id="alert-dialog-slide-title">{i18n.__('component.LogoutDialog.dialogTitle')}</DialogTitle>
      <DialogContent>
        <DialogContentText id="alert-dialog-slide-description">
          {i18n.__('component.LogoutDialog.dialogContent')}
        </DialogContentText>
        <FormGroup>
          <FormControlLabel
            control={
              <Checkbox checked={dontAsk} onChange={() => setDontAsk(!dontAsk)} name="dontAsk" color="primary" />
            }
            label={i18n.__('component.LogoutDialog.dontAskAgain')}
          />
        </FormGroup>
      </DialogContent>
      <DialogActions sx={{ justifyContent: 'space-evenly' }}>
        <Button onClick={keycloakLogout} color="primary" variant="contained">
          {i18n.__('component.LogoutDialog.buttonYes')}
        </Button>
        <Button onClick={simpleLogout} color="primary" variant="contained">
          {i18n.__('component.LogoutDialog.buttonNo')}
        </Button>
      </DialogActions>
    </Dialog>
  );
}

LogoutDialog.propTypes = {
  open: PropTypes.bool.isRequired,
  onClose: PropTypes.func.isRequired,
  onAccept: PropTypes.func.isRequired,
};

export default LogoutDialog;
