import React from 'react';
import i18n from 'meteor/universe:i18n';
import { Box, Button, Modal, Typography } from '@mui/material';
import style from './ModalStyle';

export const InputDeleteDialog = ({ confirmOpen, handleClose, handleDelete }) => {
  return (
    <Modal open={confirmOpen} onClose={handleClose}>
      <Box sx={style}>
        <Typography id="modal-modal-title" variant="h6" component="h2">
          {i18n.__('component.InputDeleteDialog.title')}
        </Typography>

        <Typography sx={{ mt: 2 }}>{i18n.__('component.InputDeleteDialog.mainText')}</Typography>

        <div style={{ display: 'flex', marginTop: '2vh', justifyContent: 'space-between' }}>
          <Button variant="contained" onClick={handleClose}>
            {i18n.__('component.InputDeleteDialog.cancel')}
          </Button>
          <Button variant="contained" onClick={handleDelete}>
            {i18n.__('component.InputDeleteDialog.confirm')}
          </Button>
        </div>
      </Box>
    </Modal>
  );
};
