import React from 'react';
import i18n from 'meteor/universe:i18n';
import Button from '@mui/material/Button';
import Modal from '@mui/material/Modal';
import Typography from '@mui/material/Typography';
import { Box } from '@mui/material';
import style from './ModalStyle';

const ModalDeleteConfirmation = ({ deleteForm, form, open, onClose }) => {
  const handleDeleteForm = async () => {
    deleteForm(form);
    onClose();
  };

  const handleCancel = () => {
    onClose();
  };

  return (
    <Modal open={open} onClose={onClose}>
      <Box sx={style}>
        <Typography id="modal-modal-title" variant="h6" component="h2">
          {i18n.__('component.modalDeleteConfirmation.title')}
        </Typography>

        <Typography sx={{ mt: 2 }}>{i18n.__('component.modalDeleteConfirmation.description')}</Typography>

        <div style={{ display: 'flex', marginTop: '2vh', justifyContent: 'space-between' }}>
          <Button variant="contained" onClick={() => handleCancel()}>
            {i18n.__('component.modalDeleteConfirmation.cancel')}
          </Button>
          <Button variant="contained" onClick={() => handleDeleteForm()}>
            {i18n.__('component.modalDeleteConfirmation.confirm')}
          </Button>
        </div>
      </Box>
    </Modal>
  );
};

export default ModalDeleteConfirmation;
