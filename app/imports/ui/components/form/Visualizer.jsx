import React, { useContext, useEffect, useState } from 'react';
import i18n from 'meteor/universe:i18n';
import { Pagination, Paper } from '@mui/material';
import { ComponentBuilder } from '../ComponentBuilder';
import SubmitAnswerForm from './SubmitAnswerForm';
import GenerateComponent from './GenerateComponent';
import { UserContext } from '../../contexts/UserContext';
import { useSelector } from 'react-redux';
import ModalRgpd from '../system/ModalRgpd';
import { FormNoAvailable } from './FormNoAvailable';

import { expirationDateIsPassed, formOnlyContainsLayoutComponent } from '../../utils/utils';

export const Visualizer = ({ answerMode = false }) => {
  const [componentToEdit] = useState({});
  const [page, setPage] = useState(1);
  const [pages, setPages] = useState(1);
  const [loader, setLoader] = useState(true);
  const [componentsByPage, setComponentsByPage] = useState([]);
  const { user } = useContext(UserContext);
  const form = useSelector((state) => state.form);

  if (!answerMode && (!form.components || form.components.length === 0))
    return <FormNoAvailable message={i18n.__('component.visualizer.formNotConform')} />;

  const preCalculateComponentsByPage = (components) => {
    let compoforpage = [];
    components.map((component, index) => {
      if (component.type === 'pageBreak' || index === components.length - 1) {
        if (index === components.length - 1 && component.type !== 'pageBreak') {
          compoforpage.push(component);
        }

        const copy = componentsByPage;
        copy.push(compoforpage);

        setComponentsByPage(copy);
        compoforpage = [];
      } else {
        compoforpage.push(component);
      }
    });
    setLoader(false);
  };

  useEffect(() => {
    setPages(form.components.filter((component) => component.type === 'pageBreak').length + 1);
    if (componentsByPage.length === 0) preCalculateComponentsByPage(form.components);
  }, [form]);

  if (!answerMode && formOnlyContainsLayoutComponent(form))
    return <FormNoAvailable message={i18n.__('component.visualizer.formNotConform')} />;
  if (!form.isActive && answerMode) return <FormNoAvailable message={i18n.__('component.visualizer.formNotActive')} />;
  if (!user && !form.isPublic) return <FormNoAvailable message={i18n.__('component.visualizer.connect')} />;
  if (answerMode && expirationDateIsPassed(form))
    return <FormNoAvailable message={i18n.__('component.visualizer.expired')} />;

  const handlePage = (event, value) => {
    setPage(value);
    window.scrollTo(0, 0);
  };

  const genComponent = (currentComponent) => (
    <div
      key={currentComponent.id}
      style={{
        display: 'flex',
        justifyContent: 'center',
        marginTop: '5vh',
        marginBottom: '2vh',
      }}
    >
      <GenerateComponent currentComponent={currentComponent} />
      {componentToEdit && componentToEdit.id === currentComponent.id && <ComponentBuilder />}
    </div>
  );

  const genCategory = (category) => {
    return (
      <Paper
        sx={{
          paddingRight: '15px',
          paddingLeft: '15px',
          backgroundColor: category[0].options?.color || 'white',
        }}
      >
        {category.map((currentComponent) =>
          Array.isArray(currentComponent) ? currentComponent : genComponent(currentComponent),
        )}
      </Paper>
    );
  };

  const genQuestionnaire = (componentsToGenerate) => {
    let questionnaire = []; // Rendu final du questionnaire
    let currentCategory = []; // Catégorie en cours de construction
    const endSection = {
      id: 'temp',
      title: 'Fin ajoutée coté client',
      type: 'sectionEnd',
    };

    // componentsToGenerate.forEach((currentComponent, currentIndex) => {
    for (let currentIndex = 0; currentIndex < componentsToGenerate.length; currentIndex++) {
      const currentComponent = componentsToGenerate[currentIndex];
      // Le composant a déjà pu être traité dans une sous catégorie
      if (currentComponent.type === 'sectionStart') {
        // Début de catégorie
        if (currentCategory.length) {
          // Catégorie déjà en cours donc on la cloture
          currentCategory.push(endSection);
          questionnaire.push(genCategory(currentCategory));
          currentCategory = [];
        }
        // Commencement d'une nouvelle catégorie
        currentCategory.push(currentComponent);
      } else if (currentComponent.type === 'sectionEnd') {
        // Fin de catégorie (code conservé pour les anciens questionnaire comportant encore des fins de sections explicites)
        if (currentCategory.length) {
          // Fin venant cloturer une catégorie en cours
          currentCategory.push(currentComponent);
          questionnaire.push(genCategory(currentCategory));
          currentCategory = [];
        } else {
          // Fin solitaire sans début correspondant => on l'ignore
        }
      } else if (currentCategory.length) {
        // Composant normal avec catégorie en cours
        currentCategory.push(currentComponent);
      } else {
        // Composant normal hors catégorie
        questionnaire.push(genComponent(currentComponent));
      }
    }
    if (currentCategory.length) {
      // Après parcours des composants, il reste une catégorie commencée mais non générée (absence de fin)
      // On l'ajoute pour un meilleur rendu
      currentCategory.push(endSection);
      questionnaire.push(genCategory(currentCategory));
    }
    return { questionnaire };
  };

  return (
    <div>
      {answerMode && form.personalDataTreatment && <ModalRgpd />}
      {<h3 style={{ textAlign: 'center' }}>{form.title}</h3>}
      {<h4 style={{ textAlign: 'center' }}>{form.description}</h4>}
      {form.firstName && (
        <h4 style={{ textAlign: 'center' }}>
          {i18n.__('component.visualizer.createdBy')} {form.firstName} {form.lastName}
        </h4>
      )}
      <div style={{ width: '59vw', margin: 'auto' }}>
        {!loader && componentsByPage && componentsByPage[page - 1]?.length > 0
          ? genQuestionnaire(componentsByPage[page - 1]).questionnaire
          : null}
        {pages > 1 ? (
          <div style={{ display: 'flex', placeContent: 'center' }}>
            <Pagination count={pages} page={page} onChange={handlePage} />
          </div>
        ) : null}
      </div>
      <br />
      {answerMode && <SubmitAnswerForm pages={pages} page={page} />}
    </div>
  );
};
