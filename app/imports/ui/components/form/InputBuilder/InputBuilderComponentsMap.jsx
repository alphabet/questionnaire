import React, { useEffect, useState } from 'react';

import { InputBuilderMap } from './InputBuilderMap';
import { useDispatch, useSelector } from 'react-redux';
import { Reorder } from 'framer-motion';
import { removeComponents, swapPositions } from '../../../redux/slices/formSlice';
import { Paper } from '@mui/material';
import { ManageComponent } from '../../ManageComponents';
import { getPaperStyle } from './utils/getPaperStyle';
import { resetQuestionObject } from '../../../redux/slices/questionSlice';
import { InputDeleteDialog } from '../../modals/InputDeleteDialog';

export const InputBuilderComponentsMap = () => {
  const form = useSelector((state) => state.form);
  const dispatch = useDispatch();

  const [localFormComponents, setLocalFormComponents] = useState(form.components);
  const [draggable, setDraggable] = useState(false);
  const [confirmOpen, setConfirmOpen] = useState(false);
  const [compToDelete, setCompToDelete] = useState(null);

  useEffect(() => {
    dispatch(swapPositions(localFormComponents));
  }, [localFormComponents]);

  useEffect(() => {
    setLocalFormComponents(form.components);
  }, [form]);

  useEffect(() => {
    // when a ManageComponent instance sets a component to delete, display confirmation dialog
    if (compToDelete !== null) setConfirmOpen(true);
  }, [compToDelete]);

  const handleDelete = () => {
    dispatch(removeComponents({ componentId: compToDelete.id }));
    dispatch(resetQuestionObject());
    setConfirmOpen(false);
    setCompToDelete(null);
  };

  return (
    <>
      <Reorder.Group
        as="div"
        values={localFormComponents}
        onReorder={(newReorder) => {
          setLocalFormComponents(newReorder);
        }}
        style={{ height: '56vh', overflow: 'auto', overflowX: 'unset' }}
      >
        {localFormComponents.map((currentComponent) => (
          <Reorder.Item
            as="div"
            drag={(draggable, 'y')}
            key={currentComponent.id}
            value={currentComponent}
            style={{ cursor: 'grab', backgroundColor: 'white' }}
          >
            <Paper sx={getPaperStyle(currentComponent)}>
              <InputBuilderMap currentComponent={currentComponent} />
              <ManageComponent
                currentComponent={currentComponent}
                setDraggable={setDraggable}
                setCompToDelete={setCompToDelete}
              />
            </Paper>
          </Reorder.Item>
        ))}
      </Reorder.Group>
      <InputDeleteDialog
        confirmOpen={confirmOpen}
        handleClose={() => {
          setConfirmOpen(false);
          setCompToDelete(null);
        }}
        handleDelete={handleDelete}
      />
    </>
  );
};
