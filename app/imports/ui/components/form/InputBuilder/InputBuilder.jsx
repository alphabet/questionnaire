import React from 'react';
import { InputBuilderLeftSide } from './InputBuilderLeftSide';
import { InputBuilderRightSide } from './InputBuilderRightSide';

const container = {
  display: 'flex',
  flexDirection: 'row',
  maxHeight: '100%',
  width: '60%',
};

export const InputBuilder = () => {
  return (
    <div style={container}>
      <InputBuilderLeftSide />
      <InputBuilderRightSide />
    </div>
  );
};
