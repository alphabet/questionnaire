import { i18n } from 'meteor/universe:i18n';
import { TextField, Typography, Alert, AlertTitle } from '@mui/material';
import React, { useContext, useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { addConsignee, addObjective, addRecipients, addEmail } from '../redux/slices/formSlice';
import { UserContext } from '../../ui/contexts/UserContext';

import 'dayjs/locale/fr';

export default function FormMentionsInputs() {
  const form = useSelector((state) => state.form);
  const { user } = useContext(UserContext);

  const dispatch = useDispatch();

  // Mandatory to be able to save the default value and be able to move on to the next step
  useEffect(() => {
    !form.consignee && dispatch(addConsignee({ consignee: `${user.firstName} ${user.lastName}` }));
    !form.email && dispatch(addEmail({ email: `${user.emails[0].address}` }));
  }, []);

  return (
    <>
      <Alert severity="info">
        <AlertTitle>{i18n.__('component.formMentionsInputs.modalities')}</AlertTitle>
        <Typography id="modal-modal-description" sx={{ mt: 2 }}>
          Vous vous apprêtez à créer un questionnaire où des données saisies par des utilisateurs sont recueillies et
          stockées.
          <br />
          De ce fait, vous devenez <b>responsable des données collectées</b> et vous engagez à{' '}
          <b>respecter les règles</b> concernant les droits des répondants. <br />
          Vous vous engagez donc à : <br />
          - n&apos;utiliser ces données que dans le cadre de l&apos;application (consultation et analyse)
          <br />- supprimer ces données au bout de <b>{Meteor.settings.public.dataDeletionDelay || 90} jours</b> suivant
          la fin d&apos;activité du questionnaire
          <br />- accéder aux demandes des utilisateurs concernant{' '}
          <b>la rectification, l&apos;effacement ou la limitation du traitement de ces données</b>
          <br />- ne demander que des informations nécessaires <b>non personnelles</b>. Certains sujets sont
          préjudiciables et donc à proscrire (religion, orientation sexuelle, convictions politiques...) <br />
          <br />
          Dans le cadre du respect de ces règles, les champs suivants sont obligatoires, et apparaîtront dans les
          mentions d&apos;informations pour toute personne remplissant le questionnaire.
          <br />
          Pour toutes informations complémentaires concernant le <b>droit des utilisateurs</b>, vous pouvez vous référer
          au site de la{' '}
          <a href="https://cnil.fr" target="_blank" rel="noopener noreferrer">
            CNIL
          </a>
          .
        </Typography>
      </Alert>

      <TextField
        id="formObjective"
        label={i18n.__('component.formMentionsInputs.formObjective')}
        variant="outlined"
        value={form.objective}
        onChange={(e) => dispatch(addObjective({ objective: e.target.value }))}
        sx={{ marginTop: '5vh' }}
        required
      />
      <TextField
        id="formConsignee"
        label={i18n.__('component.formMentionsInputs.formConsignee')}
        variant="outlined"
        defaultValue={`${user.firstName} ${user.lastName}`}
        value={form.consignee}
        onChange={(e) => dispatch(addConsignee({ consignee: e.target.value }))}
        sx={{ marginTop: '1vh' }}
        required
      />
      <TextField
        id="formEmail"
        label={i18n.__('component.formMentionsInputs.formEmail')}
        variant="outlined"
        defaultValue={`${user.emails[0].address}`}
        value={form.email}
        onChange={(e) => dispatch(addEmail({ email: e.target.value }))}
        sx={{ marginTop: '1vh' }}
        required
      />
      <TextField
        id="formRecipients"
        label={i18n.__('component.formMentionsInputs.formRecipients')}
        variant="outlined"
        value={form.recipients}
        onChange={(e) => dispatch(addRecipients({ recipients: e.target.value }))}
        sx={{ marginTop: '1vh' }}
      />
    </>
  );
}
