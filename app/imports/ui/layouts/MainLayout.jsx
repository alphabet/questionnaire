import React, { useEffect, useState } from 'react';
import { Outlet } from 'react-router-dom';
import { Header } from '../components/system/Header';

export const MainLayout = () => {
  let [theme, setTheme] = useState('eole');

  useEffect(() => {
    Meteor.call('contextsettings.getTheme', (e, r) => {
      if (e) {
        console.log(e.message);
      } else {
        setTheme(r[0]?.value[0]);
      }
    });
  }, []);

  function themeLogo(theme) {
    switch (theme) {
      case 'laboite':
        return '/images/laboite/Logo-A.svg';
      case 'eole':
        return '/images/puce_eole.png';
      case 'mim':
        return '/images/mim/logo-9.svg';
      default:
        return '/images/puce_eole.png';
    }
  }

  useEffect(() => {
    const favicon = document.getElementById('favicon');
    favicon.setAttribute('href', themeLogo(theme));
  }, [theme]);

  return (
    <>
      <Header />
      <main>
        <div style={{ marginTop: '8vh' }}>
          <Outlet />
        </div>
      </main>
    </>
  );
};
