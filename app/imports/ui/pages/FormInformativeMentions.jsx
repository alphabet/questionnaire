import React from 'react';
import i18n from 'meteor/universe:i18n';
import { useNavigate } from 'react-router-dom';
import { Paper } from '@mui/material';

import { Breadcrumb } from '../components/system/Breadcrumb';
import { Footer } from '../components/system/Footer';
import FormMentionsInputs from '../components/FormMentionsInputs';

export const FormInformativeMentions = () => {
  const navigate = useNavigate();
  const navigateTo = () => {
    navigate(`/builder/components`);
  };

  return (
    <>
      <Breadcrumb />
      <div style={{ display: 'flex', flexDirection: 'column', alignItems: 'center' }}>
        <Paper style={{ display: 'flex', flexDirection: 'column', padding: 20, width: '50%' }}>
          <FormMentionsInputs />
        </Paper>
      </div>
      <div style={{ marginTop: '8vh' }} />
      <Footer nextStep={navigateTo} urlOfPrevStep="builder/intro" text={i18n.__('page.formIntro.goNext')} />
    </>
  );
};
