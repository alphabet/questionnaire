// eslint-disable-next-line no-restricted-imports
import { Paper, Modal, Typography, Button } from '@mui/material';
import { toast } from 'react-toastify';
import { makeStyles } from 'tss-react/mui';
import React, { useState } from 'react';
import i18n from 'meteor/universe:i18n';
import Bowser from 'bowser';
import { useTheme } from '@mui/material/styles';
import PackageJSON from '../../../package.json';
import { useNavigate } from 'react-router-dom';

const useStyles = makeStyles()(() => ({
  imageSize: {
    height: '10vw',
    placeContent: 'center',
  },
  marginRight: {
    marginRight: '-10vw',
  },
  paper: {
    position: 'absolute',
    left: '50%',
    top: '50%',
    transform: 'translate(-50%, -50%)',
    padding: '5%',
  },
  containerPaper: {
    display: 'flex',
    flexDirection: 'row',
    height: '80vh',
    padding: 10,
    placeItems: 'center',
    overflow: 'auto',
  },
  imgContainer: {
    display: 'flex',
    width: '25%',
    justifyContent: 'center',
  },
  textZone: {
    width: '50vw',
  },
  footer: {
    position: 'absolute',
    bottom: 0,
    width: '100%',
  },
  textColor: {
    color: '#011CAA',
  },
}));

export const AboutPage = () => {
  const theme = useTheme();
  const [isOpen, setIsOpen] = useState(false);
  const bowser = Bowser.parse(window.navigator.userAgent);
  const { browser, os, platform } = bowser;
  const { classes } = useStyles();
  const { version } = PackageJSON;
  useNavigate();

  const handleClickModal = () => {
    navigator.clipboard.writeText(
      `Navigateur: ${browser.name},
                 Version: ${JSON.stringify(browser.version)},
                 Os: ${JSON.stringify(os.name)},
                 Appareil: ${JSON.stringify(platform.type)}`,
    );
    toast.success(i18n.__('page.aboutPage.Modal.success'));
    setIsOpen(false);
  };

  return (
    <>
      <Paper className={classes.containerPaper}>
        <div className={classes.imgContainer}>
          <img
            className={classes.imageSize}
            src={theme.palette.mode === 'dark' ? '/images/small-logo-light.svg' : '/images/puce_eole.png'}
            alt="puce eole"
          />
        </div>
        <div className={classes.textZone}>
          <Typography variant={'h3'}>
            <i className={classes.textColor} style={{ fontWeight: 'bold' }}>
              Questionnaire - version {version}
            </i>
          </Typography>
          <p>
            {i18n.__('page.aboutPage.developped')}{' '}
            <a
              className={classes.textColor}
              title="EUPL 1.2"
              target="_blank"
              rel="noreferrer noopener"
              href="https://eupl.eu/1.2/fr/"
            >
              EUPL 1.2
            </a>{' '}
            {i18n.__('page.aboutPage.socle')}{' '}
            <a
              className={classes.textColor}
              title="EOLE 3"
              target="_blank"
              rel="noreferrer noopener"
              href="https://pcll.ac-dijon.fr/eole/eole-3/"
            >
              EOLE³
            </a>
          </p>
          <p>
            {i18n.__('page.aboutPage.by')}{' '}
            <a
              className={classes.textColor}
              title="PCLL"
              target="_blank"
              rel="noreferrer noopener"
              href="https://pcll.ac-dijon.fr/pcll/"
            >
              Pôle de Compétences Logiciels Libres
            </a>{' '}
            {i18n.__('page.aboutPage.and')}{' '}
            <a
              className={classes.textColor}
              title="MENJ"
              target="_blank"
              rel="noreferrer noopener"
              href="https://www.education.gouv.fr/"
            >
              Ministère de l&apos;Éducation Nationale et de la Jeunesse
            </a>{' '}
            {i18n.__('page.aboutPage.contributions')}{' '}
            <a
              className={classes.textColor}
              title="DINUM"
              target="_blank"
              rel="noreferrer noopener"
              href="https://www.numerique.gouv.fr/dinum/"
            >
              Direction Interministérielle du Numérique
            </a>{' '}
            {i18n.__('page.aboutPage.external')}
          </p>
          <p>
            {i18n.__('page.aboutPage.links')}{' '}
            <a
              className={classes.textColor}
              title="wiki eole"
              target="_blank"
              rel="noreferrer noopener"
              href="https://eole.education/"
            >
              documentation de l&apos;application.
            </a>
          </p>
          <p>
            {i18n.__('page.aboutPage.exchange')}{' '}
            <a
              className={classes.textColor}
              title={i18n.__('page.aboutPage.chat')}
              target="_blank"
              rel="noreferrer noopener"
              href="https://matrix.mim-libre.fr/"
            >
              {i18n.__('page.aboutPage.chat')}.
            </a>
          </p>
          <p>
            {i18n.__('page.aboutPage.news')}{' '}
            <a
              className={classes.textColor}
              title="Mastodon"
              target="_blank"
              rel="noreferrer noopenner"
              href="https://mastodon.eole.education/@EOLE"
            >
              Mastodon.
            </a>
          </p>
          <p>
            {i18n.__('page.aboutPage.contributing')}{' '}
            <a
              className={classes.textColor}
              title={i18n.__('page.aboutPage.deposit')}
              target="_blank"
              rel="noreferrer noopenner"
              href="https://gitlab.mim-libre.fr/alphabet/laboite"
            >
              {i18n.__('page.aboutPage.deposit')}.
            </a>
          </p>
          <Button
            sx={{ marginTop: '5vh', backgroundColor: '#011CAA' }}
            variant="contained"
            onClick={() => setIsOpen(true)}
          >
            {i18n.__('page.aboutPage.information')}
          </Button>
        </div>
      </Paper>
      <Modal open={isOpen} onClose={() => setIsOpen(false)}>
        <Paper className={classes.paper}>
          <Typography variant="h4">{i18n.__('page.aboutPage.Modal.information')}</Typography>
          <p>
            {i18n.__('page.aboutPage.Modal.navigator')} {JSON.stringify(browser.name)}
          </p>
          <p>
            {i18n.__('page.aboutPage.Modal.version')} {JSON.stringify(browser.version)}
          </p>
          <p>
            {i18n.__('page.aboutPage.Modal.os')} {JSON.stringify(os.name)}
          </p>
          <p>
            {i18n.__('page.aboutPage.Modal.device')} {JSON.stringify(platform.type)}
          </p>
          <div style={{ display: 'flex', gap: 10 }}>
            <Button variant="contained" onClick={() => setIsOpen(false)}>
              {i18n.__('page.aboutPage.Modal.close')}
            </Button>
            <Button variant="contained" onClick={() => handleClickModal()}>
              {i18n.__('page.aboutPage.Modal.copy')}
            </Button>
          </div>
        </Paper>
      </Modal>
    </>
  );
};
