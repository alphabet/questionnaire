import React, { useState } from 'react';
import i18n from 'meteor/universe:i18n';
import { useNavigate } from 'react-router-dom';
import { MsgError } from '../components/system/MsgError';
import { Breadcrumb } from '../components/system/Breadcrumb';
import { Footer } from '../components/system/Footer';

import { useSelector } from 'react-redux';
import { InputBuilder } from '../components/form/InputBuilder/InputBuilder';
import { formOnlyContainsLayoutComponent } from '../utils/utils';

export const FormBuilder = () => {
  const today = new Date();
  const [errorMessage, setErrorMessage] = useState('');
  const form = useSelector((state) => state.form);
  const navigate = useNavigate();
  const isDisable = !form.title || form.components.length === 0;
  const wrongExpirationDate = today > form.expirationDate;
  const haveErrorMessages = !!errorMessage.length;

  const navigateToNextStep = () => {
    if (isDisable) {
      setErrorMessage(i18n.__('component.componentBuilder.errors.noTitleOrOptions'));
    } else if (wrongExpirationDate) {
      setErrorMessage(i18n.__('component.componentBuilder.errors.expirationDateInvalid'));
    } else if (formOnlyContainsLayoutComponent(form)) {
      setErrorMessage(i18n.__('component.componentBuilder.errors.onlyLayoutComponent'));
    } else {
      navigate('/builder/previsualizer');
    }
  };

  return (
    <>
      <Breadcrumb />
      <div style={{ display: 'flex', justifyContent: 'center' }}>
        <InputBuilder />
      </div>
      {haveErrorMessages && <MsgError message={errorMessage} setMessage={setErrorMessage} />}
      <div style={{ marginTop: '8vh' }} />
      <Footer
        nextStep={navigateToNextStep}
        urlOfPrevStep={form.personalDataTreatment ? 'builder/rgpd' : 'builder/intro'}
        text={i18n.__('page.formIntro.goNext')}
      />
    </>
  );
};
