import { Meteor } from 'meteor/meteor';
import React, { createContext, useEffect, useState } from 'react';
import i18n from 'meteor/universe:i18n';
import { useTracker } from 'meteor/react-meteor-data';
import UserFailed from '../components/system/UserFailed';

export const UserContext = createContext();

export const UserProvider = ({ children }) => {
  const [userFailed, setUserFailed] = useState(false);
  const stopCallback = Accounts.onLoginFailure((details) => {
    if (details.error.error === 'api.users.createUser') {
      setUserFailed(true);
    }
  });
  useEffect(() => {
    return () => {
      if (typeof stopCallback.stop === 'function') {
        stopCallback.stop();
      }
    };
  }, []);

  const isLoading = useTracker(() => {
    const userHandle = Meteor.subscribe('userData');
    return !userHandle.ready();
  });
  const user = useTracker(() => {
    return Meteor.user();
  });

  const isAuthenticated = !!user;

  useEffect(() => {
    if (user && !isLoading) {
      // set default language to fr if user doesn't have set language before
      i18n.setLocale(user.language || 'fr');
      document.documentElement.setAttribute('lang', user.language);
    }
  }, [user]);

  return (
    <UserContext.Provider value={{ user, isLoading, isAuthenticated, userFailed }}>
      {userFailed ? <UserFailed setUserFailed={setUserFailed} /> : children}
    </UserContext.Provider>
  );
};

// Creer un hook perso.
