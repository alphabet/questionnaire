export const isDuplicate = (allOpts, newOption) => {
  return allOpts.includes(newOption);
};

export const hasNotAnswers = (form) => {
  if (!form.formAnswers || form.formAnswers.length === 0) return true; // formAnswers peut etre initialisé a tableau vide pour retirer la 1ere condition !?
  return false;
};

export const isFormActive = (form) => {
  return form.active;
};

export const toggleActiveForm = async (form) => {
  form.active = !form.active;
  await Meteor.callAsync('forms.toggleActive', form._id, form.active);
};

export const hasAlreadyRespond = (user, form) => {
  if (hasNotAnswers(form)) return false;
  if (!user) return false;
  const { formAnswers } = form;
  return !!formAnswers.find((answer) => answer.userId === user._id);
};

export const expirationDateIsPassed = (form) => {
  return form.expirationDate < new Date();
};

export const copyUrlToClipBoard = (id) => {
  const url = `${Meteor.absoluteUrl()}visualizer/${id}`;
  return navigator.clipboard.writeText(url);
};

export const generateColor = () => {
  let r = Math.random() * 255;
  let g = Math.random() * 255;
  let b = Math.random() * 255;

  return `rgba(${r},${g},${b}, .4)`;
};

export const checkIntegrityOfForm = (form) => {
  if (form.components.length === 0) return false;

  let isValid = false;

  isValid = form.components.some(
    (component) =>
      component.type !== 'sectionStart' &&
      component.type !== 'sectionEnd' &&
      component.type !== 'separator' &&
      component.type !== 'pageBreak',
  );

  return isValid;
};

export const IsLayoutComponent = (component) => {
  return (
    component.type === 'pageBreak' ||
    component.type === 'separator' ||
    component.type === 'sectionEnd' ||
    component.type === 'sectionStart' ||
    component.type === 'comment'
  );
};

export const formOnlyContainsLayoutComponent = (form) => {
  return !form.components?.some((component) => !IsLayoutComponent(component));
};

export const openInNewTab = (item) => {
  if (typeof item === 'string') {
    window.open(item, '_blank').focus();
  } else {
    window.open(item.path, '_blank').focus();
  }
};
